Thank you for wanting to submit a report and help me improve CloudRig. Please don't write your bug report from scratch! I've put buttons in the Blender interface which will start a report for you with a lot of  useful information pre-filled.

- If your bug is about a metarig that fails to generate:
    Select your metarig and go to Properties->Object Data->Rigify Log, and click on "Report CloudRig Bug".
- For all other bugs:
    Go in the Rigify Addon preferences, select the CloudRig feature set and click on "Report a Bug."
- For feature suggestions:
    In this case you may delete this text and write from scratch.