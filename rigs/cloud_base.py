# Typing
import bpy
from ..bone import BoneInfo
from typing import Dict, List

# CloudBaseRig parent classes
from rigify.base_rig import BaseRig
from ..bone import BoneSetMixin
from ..utils.ui import CloudUIMixin
from ..utils.mechanism import CloudMechanismMixin
from ..utils.animation import CloudAnimationMixin
from ..utils.object import CloudObjectUtilitiesMixin

# The rest
from bpy.props import BoolProperty, StringProperty, CollectionProperty, PointerProperty, IntProperty, EnumProperty
from mathutils import Vector
from enum import Enum
from ..parent_switching import draw_cloudrig_parents, ParentSlot
from ..utils.ui import draw_label_with_linebreak

class DefaultLayers(Enum):
	IK_MAIN = 0
	IK_SECOND = 16
	FK_MAIN = 1
	FK_SECOND = 17

	STRETCH = 2
	DEF_CTR = 18

	FACE_MAIN = 3
	FACE_SECOND = 19

	DEF = 29
	MCH = 30
	ORG = 31

	FACE_TWEAK = 20

class CloudBaseRig(
					BaseRig,
					CloudMechanismMixin,
					CloudObjectUtilitiesMixin,
					CloudUIMixin,
					CloudAnimationMixin,
					BoneSetMixin
	):
	"""Base class that all CloudRig rigs should inherit from."""

	default_layers = lambda name: DefaultLayers[name].value

	# Strings to try to communicate obscure behaviours of this rig type in the params UI.
	use_custom_props = False	# TODO: Instead of an awkward "feauter exists or not" flag like this, we should split these features off into a compositable class, eg. utils.custom_properties->CloudCustomPropertyMixin.
	custom_prop_behaviour = 'Store Custom Properties for this rig element in a cogwheel shaped bone at the base of the rig.'
	relinking_behaviour = 'Metarig constraints can specify a target bone name after an "@" symbol in the constraint name.'
	parent_switch_behaviour = 'The active parent will own the rig\'s root bone.'
	parent_switch_overwrites_root_parent = True

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		### Quick access to the generator's name manager
		self.naming = self.generator.naming

		### Quick access to the generator's log manager
		self.logger = self.generator.logger

		# Determine Suffix/Prefix
		self.side_suffix = ""
		self.side_prefix = ""
		is_left = self.naming.side_is_left(self.base_bone)
		if is_left:
			self.side_suffix = "L"
			self.side_prefix = "Left"
		elif is_left==False:
			self.side_suffix = "R"
			self.side_prefix = "Right"

	def find_org_bones(self, pose_bone):
		"""Populate self.bones.org.main."""

		chain = self.get_rigify_chain(pose_bone)
		from rigify.utils.bones import BoneDict
		return BoneDict(main=[b.name for b in chain])

	def initialize(self):
		"""Gather and validate data about the rig."""
		super().initialize()

		from .. import cloud_generator
		assert type(self.generator) == cloud_generator.CloudGenerator, "CloudRig has wrong Generator type. CloudRig requires its own Generator class - You're probably using bpy.ops.rigify_generate instead of bpy.ops.cloudrig_generate. Perhaps the Generate button is not being replaced even though it should?"

		self.generator_params = self.generator.metarig.data

		self.mch_disable_select = not self.generator_params.cloudrig_parameters.mechanism_selectable

		self.meta_base_bone = self.generator.metarig.pose.bones.get(self.base_bone.replace("ORG-", ""))

		self.scale = self.generator.scale

		# Prepare Bone Sets
		self.bone_sets = []	# TODO: This is currently not used, but it may be turned into a dictionary in future.
		self.defaults = dict(self.generator.defaults)
		self.ensure_bone_sets()

		parent = self.get_bone(self.base_bone).parent
		self.bones.parent = parent.name if parent else ""

		# Get a reference to the Root bone from the generator.
		self.root_bone = None
		if self.generator_params.cloudrig_parameters.create_root:
			self.root_bone = self.generator.root_bone

		self.update_forced_params()

	def update_forced_params(self):
		clas = type(self)
		for param in clas.forced_params.keys():
			forced_value = clas.forced_params[param]
			if forced_value != 'NOFORCE':
				self.meta_base_bone.rigify_parameters[param] = forced_value
				setattr(self.params, param, forced_value)

	@property
	def properties_bone(self) -> BoneInfo:
		"""Ensure that a Properties bone exists, and return it."""
		# This is a @property so that if it's never called(like in the case of very simple rigs), the properties bone is not created.
		# https://en.wikipedia.org/wiki/Lazy_initialization

		if self.params.CR_base_props_storage == 'CUSTOM':
			prop_bone_name = self.params.CR_base_props_storage_bone
			properties_bone = self.generator.find_bone_info(prop_bone_name)
			if not properties_bone:
				self.add_log("Custom Property bone not found"
					,trouble_bone = prop_bone_name
					,description = f"Custom Property bone named {prop_bone_name} not found, falling back to default Properties bone. If it exists, make sure it generates before this rig."
				)
				self.params.CR_base_props_storage = 'DEFAULT'
			else:
				return properties_bone

		if self.params.CR_base_props_storage == 'DEFAULT':
			bone_name = "Properties"
			properties_bone = self.generator.find_bone_info(bone_name)
			if not properties_bone:
				properties_bone = self.generator.root_set.new(
					name		  = bone_name
					,head		  = Vector((0, self.scale*2, 0))
					,tail		  = Vector((0, self.scale*2, self.scale*2))
					,bbone_width  = 1/8
					,custom_shape = self.ensure_widget("Cogwheel_Y")
					,use_custom_shape_bone_size = True
				)
			return properties_bone
		elif self.params.CR_base_props_storage == 'GENERATED':
			# Create a bone at the base of the rig with a cogwheel shape.
			properties_bone = self.generate_properties_bone()
			# This block should only run once, so change the storage type to no longer be 'GENERATED'.
			self.params.CR_base_props_storage = 'CUSTOM'
			self.params.CR_base_props_storage_bone = properties_bone.name
			return properties_bone

	def generate_properties_bone(self) -> BoneInfo:
		org_bone = self.org_chain[0]
		properties_bone = self.mch_bones.new(
			name		  = org_bone.name.replace("ORG", "PRP")
			,source 	  = org_bone
			,parent		  = org_bone
			,custom_shape = self.ensure_widget("Cogwheel_Y")
			,use_custom_shape_bone_size = True
		)
		properties_bone.layers = self.meta_base_bone.bone.layers[:]
		return properties_bone

	def ensure_bone_sets(self):
		self.def_chain = self.ensure_bone_set("Deform Bones")
		self.mch_bones = self.ensure_bone_set("Mechanism Bones")
		self.org_chain = self.ensure_bone_set("Original Bones")

	def prepare_bones(self):
		self.create_bone_infos()
		if self.params.CR_base_parent_switching:
			self.apply_parent_switching()
		if self.params.CR_base_parent!="":
			self.apply_custom_root_parent()
		if self.params.CR_base_relink:
			self.relink()

	def create_bone_infos(self):
		self.load_org_bone_infos()

	def apply_parent_switching(self,
			child_bone=None,
			prop_bone=None, prop_name="",
			ui_area="misc_settings", row_name="", col_name=""
		):
		"""Rig a bone with multiple switchable parents, using Armature constraint and drivers."""
		if not child_bone:
			child_bone = self.org_chain[0]
		if not prop_bone:
			prop_bone = self.properties_bone
		if prop_name=="":
			prop_name="parents_"+child_bone.name
		if row_name=="":
			row_name = child_bone.name.split(".")[0]
		if col_name=="":
			col_name = child_bone.name

		# Create parent bone that will hold the Armature constraint.
		arm_con_bone = self.create_parent_bone(child_bone, self.mch_bones)
		arm_con_bone.hide_select = self.mch_disable_select
		arm_con_bone.name = "Parents_" + child_bone.name
		arm_con_bone.custom_shape = None

		parent_names, parent_bones = self.collect_parents()
		targets = [{'subtarget' : bone_name} for bone_name in parent_bones]

		# Create custom property
		info = {
			"prop_bone" : prop_bone,
			"prop_id" : prop_name,
			"texts" : parent_names,

			"operator" : "pose.cloudrig_switch_parent_bake",
			"icon" : "COLLAPSEMENU",
			"parent_names" : parent_names,
			"bones" : [child_bone.name],
			}
		self.add_ui_data(ui_area, row_name, col_name, info, default=0, max=len(parent_names)-1)

		# Add armature constraint
		arm_con = arm_con_bone.add_constraint('ARMATURE',
			targets = targets
		)

		# Add weight drivers
		for i, t in enumerate(arm_con.targets):
			arm_con.drivers.append({
				'prop' : f'targets[{i}].weight',
				'expression' : f'parent=={i}',
				'variables' : {
					'parent' : {
						'type' : 'SINGLE_PROP',
						'targets' : [{
							'data_path' : f'pose.bones["{prop_bone.name}"]["{prop_name}"]'
						}]
					}
				}
			})

	def collect_parents(self) -> (List[str], List[str]):
		"""Gather parent information and check for issues.

		Returns two lists of equal length, first one is the UI name second is the bone name of each parent.
		"""

		parent_bones = []
		parent_names = []

		parent_slots = self.params.CR_base_parent_slots
		for i, ps in enumerate(parent_slots):
			if ps.bone=="":
				self.add_log(
					"Parent not found"
					,description=f"Parent slot #{i}: {ps.bone} not specified, skipping."
				)
				continue
			if ps.name=="":
				self.add_log(
					"Nameless parent"
					,description = f"Parent slot #{i}: {ps.bone} has no UI name, falling back to the bone's name."
				)
				parent_names.append(ps.bone)
			else:
				parent_names.append(ps.name)
			parent_bones.append(ps.bone)
		if len(parent_names)==1:
			self.add_log("No parents found"
				,description = f"No parents specified for parent switching setup, skipping completely."
			)
			return [], []

		if self.generator_params.cloudrig_parameters.create_root and 'root' not in parent_bones:
			parent_names.insert(0, "Root")
			parent_bones.insert(0, 'root')

		return parent_names, parent_bones

	def apply_custom_root_parent(self, bone=None, parent_name=""):
		if not bone:
			bone = self.org_chain[0]
		if parent_name=="":
			parent_name = self.params.CR_base_parent
		self.bendy_parenting(bone, parent_name)

	def relink(self):
		# Relink the base bone.
		bi = self.org_chain[0]
		bi.relink()

	def reparent_bone(self, child: BoneInfo):
		# TODO: This should be deprecated. It's only used by cloud_aim, which should instead rely on apply_custom_root_parent!
		"""Overriding from CloudMechanismMixin just for an extra sanity check."""
		parent = super().reparent_bone(child)

		assert parent in self.org_chain, f"Cannot reparent {child}, its parent, {child.parent} was expected to be an ORG bone of rig {self.base_bone}"
		return parent

	def load_org_bone_infos(self):
		"""Read ORG bones into BoneInfo instances in self.org_chain."""

		for bn in self.bones.org.main:
			eb = self.get_bone(bn)
			eb.use_connect = False

			meta_org_name = eb.name[4:]
			meta_org = self.generator.metarig.pose.bones.get(meta_org_name)

			if self.naming.has_trailing_zeroes(meta_org):
				self.add_log("Trailing Zeroes!"
					,trouble_bone = eb.name
					,operator = 'object.cloudrig_rename_bone'
					,op_kwargs = {'old_name' : meta_org_name}
				)

			org_bi = self.org_chain.new_from_real(self.obj, eb)
			org_bi.layers = self.org_chain.layers[:]
			org_bi.hide_select = self.mch_disable_select
			org_bi.bbone_width = eb.bbone_x / self.scale
			if eb.parent:
				parent = self.generator.find_bone_info(eb.parent.name)
				org_bi.parent = parent

			# TODO: arbitrary property assignment, should be avoided!
			org_bi.meta_bone = meta_org

	def add_log(self
			,description_short
			,**kwargs
		):
		kwargs['owner_bone'] = self.meta_base_bone.name
		self.generator.logger.log(description_short ,**kwargs)

	def add_log_bug(self
			,description_short
			,**kwargs
		):
		kwargs['owner_bone'] = self.meta_base_bone.name
		self.generator.logger.log_bug(description_short ,**kwargs)

	# TODO these functions probably belong to the generator and should be called get_ instead of find_.
	def find_symmetry_rig(self) -> BaseRig:
		"""Find another rig in the generator with the opposite name for self.base_bone."""
		flipped_name = self.naming.flipped_name(self.base_bone)
		if flipped_name == self.base_bone: return

		for rig in self.generator.rig_list:
			if rig.base_bone == flipped_name:
				return rig

	def find_sibling_rigs(self) -> List[BaseRig]:
		siblings = []
		for rig in self.generator.rig_list:
			if rig.rigify_parent == self.rigify_parent:
				siblings.append(rig)

		return siblings

	##############################
	# Parameters

	@classmethod
	def add_parameters(cls, params):
		"""Add rig parameters to the RigifyParameters PropertyGroup."""

		params.CR_base_show_settings = BoolProperty(
			name		 = "Base Settings"
			,description = "Reveal settings for the cloud_base rig type"
		)
		params.CR_base_parent_switching = BoolProperty(
			name		 = "Multiple Parents"
			,description = "Use parent switching for this rig. Different rig types may implement this differently. A rig type specific explanation is shown below when this is enabled"
			,default	 = False
		)
		params.CR_base_relink = BoolProperty(
			name		 = "Relink Constraints"
			,description = "Constraints and drivers on this rig will be relinked to the corresponding primary controls that are created for each bone. This can be different for each rig type, see the description text below or right click and click Open Manual"
			,default	 = True
		)
		params.CR_base_parent = StringProperty(
			name		 = "Root Parent"
			,description = "If specified, parent the root of this rig to the chosen bone"
			,default	 = ""
		)
		params.CR_base_parent_slots = CollectionProperty(type=ParentSlot)
		params.CR_base_active_parent_slot_index = IntProperty()

		params.CR_base_props_storage = EnumProperty(
			name		 = "Custom Property Storage"
			,items		 = [
				('DEFAULT', "Default", 'Use a shared bone called "Properties"')
				,('CUSTOM', "Custom", "Select an existing bone")
				,('GENERATED', "Generated", "Generate a bone specifically for this rig element. This can be implemented differently by different rig types")
			]
		)
		params.CR_base_props_storage_bone = StringProperty(
			name		 = "Properties Bone"
			,description = 'Store custom properties in the chosen bone. If empty, will fall back to a bone called "Properties"'
			,default	 = ""
		)

		cls.define_bone_sets(params)

	@classmethod
	def define_bone_sets(cls, params):
		"""Create parameters for this rig's bone sets."""
		super().define_bone_sets(params)
		params.CR_show_bone_sets = BoolProperty(name="Bone Sets")

		cls.define_bone_set(params, "Deform Bones",		default_layers=[cls.default_layers('DEF')], override='DEF')
		cls.define_bone_set(params, "Mechanism Bones",	default_layers=[cls.default_layers('MCH')], override='MCH')
		cls.define_bone_set(params, "Original Bones",	default_layers=[cls.default_layers('ORG')], override='ORG')

	@classmethod
	def parameters_ui(cls, layout, params):
		"""Create the ui for the rig parameters."""

		layout = cls.draw_cloud_params(layout, bpy.context, params)
		layout.separator()
		cls.draw_bone_sets_params(layout, params)

	@classmethod
	def draw_cloud_params(cls, layout, context, params):
		"""Create the ui for the rig parameters."""
		layout = super().draw_cloud_params(layout, context, params)

		if not cls.draw_dropdown_menu(layout, params, "CR_base_show_settings"): return layout

		relink_box = layout.box()
		cls.draw_prop(relink_box, params, "CR_base_relink")
		if params.CR_base_relink:
			draw_label_with_linebreak(relink_box, cls.relinking_behaviour, align_split=True)

		metarig = context.object
		rig = metarig.data.rigify_target_rig

		if not rig:
			draw_label_with_linebreak(layout, "Generate the rig to see more parameters.", align_split=True)
			return layout

		parent_box = layout.box()
		parent_bone = rig.pose.bones.get(params.CR_base_parent)
		cls.draw_prop(parent_box, params, "CR_base_parent_switching")
		if params.CR_base_parent!="" and not parent_bone:
			cls.draw_prop_search(parent_box, params, 'CR_base_parent', rig.pose, 'bones', icon='ERROR')
			draw_label_with_linebreak(parent_box, "Bone no longer exists in rig!", align_split=True)
		elif not (cls.parent_switch_overwrites_root_parent and params.CR_base_parent_switching):
			cls.draw_prop_search(parent_box, params, 'CR_base_parent', rig.pose, 'bones')
			if parent_bone and parent_bone.bone.bbone_segments > 1:
				draw_label_with_linebreak(parent_box, "Bendy Bone, will use Armature Constraint and create a parent helper bone!", align_split=True)

		if params.CR_base_parent_switching:
			draw_label_with_linebreak(parent_box, cls.parent_switch_behaviour, align_split=True)
			draw_cloudrig_parents(parent_box, context.active_pose_bone)

		if cls.use_custom_props:
			cls.draw_custom_prop_params(layout, context, params)

		return layout

	@classmethod
	def draw_custom_prop_params(cls, layout, context, params):
		metarig = context.object
		rig = metarig.data.rigify_target_rig
		custom_props_box = layout.box()
		cls.draw_prop(custom_props_box, params, "CR_base_props_storage", expand=True)
		if params.CR_base_props_storage == 'CUSTOM':
			cls.draw_prop_search(custom_props_box, params, 'CR_base_props_storage_bone', rig.pose, 'bones')
		elif params.CR_base_props_storage == 'GENERATED':
			draw_label_with_linebreak(custom_props_box, cls.custom_prop_behaviour, align_split=True)